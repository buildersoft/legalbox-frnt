import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../component/Country';

@Injectable({
  providedIn: 'root'
})

export class CountryService {
  // http://localhost:8080/api/category/1.0/listTemplates/5439c4787ed711eba9bf0800271e99ca
  private contextPath: string = 'http://localhost:8080/api/country/1.0';
  private listAllPoint: string = '/listAll';
  //  private listTemplatesEndpoint: string = "/listTemplates";

  constructor(private http: HttpClient) {
  }

  listAll(): Observable<Country[]> {
    return this.http.get<Country[]>(this.contextPath + this.listAllPoint);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Participant } from '../component/Participant';
import { NewDocument } from '../component/NewDocument';
import { Document } from '../component/Document';

@Injectable({
  providedIn: 'root'
})

export class DocumentService {
  private contextPath: string = 'http://localhost:8080/api/document/1.0';
  private createDocumentUrl: string = '/createDocument';
  private readDocumentUrl: string = '/readDocument';
  private updateDocumentUrl: string = '/updateDocument';
  private listByUserDocumentUrl: string = '/listByUser';

  constructor(private http: HttpClient) {
  }

  createDocument(userName: string, template: string, participants: Participant[]): Observable<Document> {
    //    console.log('asi no mas');
    let newDocument: NewDocument = new NewDocument();
    newDocument.templateId = template;
    newDocument.participants = participants;
    newDocument.userName = userName;

    console.log(JSON.stringify(newDocument));

    return this.http.post<Document>(this.contextPath + this.createDocumentUrl, newDocument);
  }

  readDocument(documentId: string): Observable<Document> {
    let readDocumentUrlTemp = this.contextPath + this.readDocumentUrl + `/${documentId}`;
    //  console.log('ReadDocumentUrlTemp: ', readDocumentUrlTemp);

    return this.http.get<Document>(readDocumentUrlTemp);
  }

  updateDocument(document: Document): Observable<Document> {
    let updateDocumentUrlTemp = this.contextPath + this.updateDocumentUrl;
    console.log('updateDocument' + JSON.stringify(document));
    return this.http.post<Document>(updateDocumentUrlTemp, document);
  }

  listDocuments(user: string): Observable<Document[]> {
    let listDocumentsUrlTemp = this.contextPath + this.listByUserDocumentUrl + `/${user}`;
    return this.http.get<Document[]>(listDocumentsUrlTemp);
  }
}

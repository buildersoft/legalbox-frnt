// http://localhost:8080/api/identityType/1.0/listAll
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class IdentityTypeService {
  // http://localhost:8080/api/category/1.0/listTemplates/5439c4787ed711eba9bf0800271e99ca
  private contextPath: string = 'http://localhost:8080/api/identityType/1.0';
  private listAllIdentities: string = '/listAll';
  //  private listTemplatesEndpoint: string = "/listTemplates";

  constructor(private http: HttpClient) {
  }

  listAll(): Observable<string[]> {
    return this.http.get<string[]>(this.contextPath + this.listAllIdentities);
  }
}

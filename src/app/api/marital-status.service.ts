import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MaritalStatus } from '../component/marital-status';

@Injectable({
  providedIn: 'root'
})

export class MaritalStatusService {
  // http://localhost:8080/api/category/1.0/listTemplates/5439c4787ed711eba9bf0800271e99ca
  private contextPath: string = 'http://localhost:8080/api/marital-status/1.0';
  private listAllPoint: string = '/listAll';
  //  private listTemplatesEndpoint: string = "/listTemplates";

  constructor(private http: HttpClient) {
  }

  listAll(): Observable<MaritalStatus[]> {
    return this.http.get<MaritalStatus[]>(this.contextPath + this.listAllPoint);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LegalMatter } from '../component/LegalMatter';
import { Template } from '../component/Template';

@Injectable({
  providedIn: 'root'
})

export class MatterService {
  // http://localhost:8080/api/category/1.0/listTemplates/5439c4787ed711eba9bf0800271e99ca
  private contextPath: string = 'http://localhost:8080/api/category/1.0';
  private listAllLegalMatter: string = '/listAllLegalMatter';
  private listTemplatesEndpoint: string = "/listTemplates";

  constructor(private http: HttpClient) {
  }

  listMatter(): Observable<LegalMatter[]> {
    return this.http.get<LegalMatter[]>(this.contextPath + this.listAllLegalMatter);
  }

  listTemplates(uuid: string): Observable<Template[]> {
    let url = this.contextPath + this.listTemplatesEndpoint + "/" + uuid;
//    console.log(url);
    return this.http.get<Template[]>(url);
  }

}

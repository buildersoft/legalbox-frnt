// CL-B6DD01FB-8007-11EB-9436-0800271E99CA
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Person } from '../component/Person';

@Injectable({
  providedIn: 'root'
})

export class ParticipantService {
  private contextPath: string = 'http://localhost:8080/api/participant/1.0';
  private findUrl: string = '/find';
  private findByIdentityUrl: string = '/findByIdentity';

  constructor(private http: HttpClient) {
  }

  find(blpid: string): Observable<Person> {
    return this.http.get<Person>(this.contextPath + this.findUrl + '/' + blpid);
  }
  
  findByIdentity(identityType: string, identityNumber: string): Observable<Person> {
    // http://localhost:8080/api/participant/1.0/findByIdentity
    return this.http.get<Person>(this.contextPath + this.findByIdentityUrl + '/' + identityType + '/' + identityNumber);
  }

  /*
    listMatter(): Observable<LegalMatterDTO[]> {
      return this.http.get<LegalMatterDTO[]>(this.contextPath + this.listAllLegalMatter);
    }

    listTemplates(uuid: string): Observable<TemplateDTO[]> {
      let url = this.contextPath + this.listTemplatesEndpoint + "/" + uuid;
      console.log(url);
      return this.http.get<TemplateDTO[]>(url);
    }
  */
}

import { Injectable } from '@angular/core';
import { Natural } from '../component/Natural';
import { Participant } from '../component/Participant';
import { Person } from '../component/Person';

@Injectable({
    providedIn: 'root'
})

export class ParticipantServiceUtil {
    constructor() { }
    partTypes: string[] = null;

    setPartTypes(partTypes: string[]) {
        this.partTypes = partTypes;
    }

    personToParticipant(person: Person): Participant {
        console.log(person);
        
        let out: Participant = new Participant();
        Object.assign(out, person);
//        out = Object.assign(person, out);
        out.partType = 1;
        out.partTypeName = !this.partTypes ? '' : this.partTypes[0];
        return out;
    }

    removeFromParticipants(participants: Participant[], participant: Participant): Participant[] {
        let localParticipants: Participant[] = [];
        console.log(participant + "------" + localParticipants);

        localParticipants = participants.filter(current => participant.blpid != current.blpid);
        //        localParticipants = participants.filter(current => !this.areEquals(participant, current));

        return localParticipants;
    }

    areEquals(p1: Participant, p2: Participant): boolean {
        return this.identityEquals(
            p1.natural.documentIdentityType,
            p1.natural.documentIdentityNumber,
            p2.natural.documentIdentityType,
            p2.natural.documentIdentityNumber);

        // return p1.natural.documentIdentityType == p2.natural.documentIdentityType &&
        //     p1.natural.documentIdentityNumber == p2.natural.documentIdentityNumber;
    }

    identityEquals(type1: string, number1: string, type2: string, number2: string): boolean {
        const out: boolean = (type1 == type2 && number1 == number2);
        return out;
    }

    existsParticipant(participants: Participant[], participant: Participant): boolean {
        let out: boolean = false;
        participants.forEach(p => {
            if (!out && this.areEquals(p, participant)) {
                out = true;
            }
        })
        return out;
    }

    retrieveParticipantByIdentity(participants: Participant[], documentIdentityType: string, documentIdentityNumber: string): Participant {
        let participant: Participant = null;

        participants.forEach(p => {
            if (this.identityEquals(p.natural.documentIdentityType, p.natural.documentIdentityNumber, documentIdentityType, documentIdentityNumber)) {
                participant = { ...p };
            }
        });

        return participant;
    }

    createNewParticipant(): Participant {
        let out: Participant = new Participant();
        out.natural = new Natural();
        out.natural.documentIdentityType = 'RUT';
        out.natural.documentIdentityNumber = '';
        //    out.natural.nationalityUuid = this.countries[0].id;
        out.partType = 1;
        out.personType = 'NATURAL';
        return out;
    }

}

// CL-B6DD01FB-8007-11EB-9436-0800271E99CA
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
import { User } from '../component/User';
import { Person } from '../component/Person';
import { Natural } from '../component/Natural';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NewUser } from '../component/new-user';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private appUser: string = 'legalbox';
  private appPassword: string = 'alguna-clave';
  private urlRequest: string = 'http://localhost:8080/oauth/token';
  private baseUrl: string = 'http://localhost:8080/api/user/1.0';
  private newUserUrl: string = this.baseUrl + '/createNewUser';
  private activateUserUrl: string = this.baseUrl + '/activeUser';

  private _token: string;
  private _user: User;

  private ACCESS_TOKEN: string = 'AccessToken';
  private USER: string = 'User';

  constructor(
    private http: HttpClient,
    private router: Router) { }

  public get user(): User {
    if (this._user != null) {
      return this._user;
    } else if (this._user == null && sessionStorage.getItem(this.USER) != null) {
      this._user = JSON.parse(sessionStorage.getItem(this.USER)) as User;
      return this._user;
    }
    return new User();
  }
  getUserAsPerson(): Person {
    let person: Person = new Person();

    let u: User = this.user;

    person.blpid = u.blpid;
    person.natural.address = u.address;
    person.natural.birthday = u.birthday;
    person.natural.documentIdentityNumber = u.documentIdentityNumber;
    person.natural.documentIdentityType = u.documentIdentityType;
    person.natural.email = u.email;
    person.natural.lastNames = u.lastNames;
    person.natural.names = u.names;
    person.natural.nationalityId = u.nationalityId;
    person.natural.nationalityName = u.nationalityName;
    person.natural.email = u.username;

    return person;
  }
  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && sessionStorage.getItem(this.ACCESS_TOKEN) != null) {
      this._token = sessionStorage.getItem(this.ACCESS_TOKEN);
      return this._token;
    }
    return null;
  }

  logout(): void {
    this._user = null;
    this._token = null;
    sessionStorage.clear();
  }

  login(username: string, password: string): Observable<any> {
    const credentials: string = btoa(this.appUser + ':' + this.appPassword);
    const httpHeaders = this.getHeaders(credentials);

    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', username);
    params.set('password', password);

    return this.http.post<any>(this.urlRequest, params.toString(), { headers: httpHeaders });
  }


  private getHeaders(credentials: string) {
    return new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic ' + credentials });
  }

  saveToken(accessToken: string): void {
    this._token = accessToken;
    sessionStorage.setItem(this.ACCESS_TOKEN, accessToken);
  }

  saveUser(accessToken: string): void {
    let payload = this.getTokenData(accessToken);
    console.log('Payload: ' + JSON.stringify(payload));

    this._user = new User();
    this._user.username = payload.user_name;
    this._user.roles = payload.authorities;

    //    this._user.blpid = payload.blpid;
    this._user.address = payload.address;
    this._user.birthday = payload.birthday;
    this._user.documentIdentityNumber = payload.documentIdentityNumber;
    this._user.documentIdentityType = payload.documentIdentityType;
    this._user.email = payload.email;
    this._user.lastNames = payload.lastNames;
    this._user.names = payload.names;
    this._user.nationalityId = payload.nationalityId;
    this._user.nationalityName = payload.nationalityName;
    this._user.blpid = payload.blpid;

    sessionStorage.setItem(this.USER, JSON.stringify(this._user));
  }

  getTokenData(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split(".")[1]));
    }
    return null;
  }

  isAuthenticated(): boolean {
    // console.log(sessionStorage.getItem(this.ACCESS_TOKEN)!=null);
    let user = this.getTokenData(this.token);
    return user != null && user.user_name && user.user_name.length > 0;   // sessionStorage.getItem(this.ACCESS_TOKEN) != null;
  }

  createNewUser(newUser: NewUser): Observable<User> {
    return this.http.post<User>(this.newUserUrl, newUser);
  }

  activateUser(userId: string) {
    return this.http.get<User>(this.activateUserUrl + '/' + userId);
  }
}

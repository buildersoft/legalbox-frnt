import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { NavbarComponent } from './gui/navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MatterService } from './api/matter.service';
import { ParticipantService } from './api/participant.service';
import { IdentityTypeService } from './api/identityType.service';
import { CountryService } from './api/country.service';
import { UserService } from './api/user.service';
import { DocumentService } from './api/document.service';
import { ParticipantServiceUtil } from './api/participant.util';
import { MaritalStatusService } from './api/marital-status.service';

// import { routing } from './app.routing';
import { StepOneComponent } from './gui/stepOne/stepOne.component';
import { RouterModule, Routes } from '@angular/router';
import { ParagrafoComponent } from './gui/paragrafo/paragrafo.component';
import { TbkComponent } from './gui/tbk/tbk.component';
import { DocumentListComponent } from './gui/document-list/document-list.component';
import { LoginComponent } from './gui/login/login.component';
import { RequestGuard } from './api/request-guard/request.guard';
import { TokenInterceptor } from './api/interceptor/token.interceptor';
import { ViewDocumentComponent } from './gui/view-document/view-document.component';
import { RegistroComponent } from './gui/registro/registro.component';
import { RegistradoComponent } from './gui/registrado/registrado.component';


const routes: Routes = [
  { path: 'stepOne', component: StepOneComponent, canActivate: [RequestGuard] },
  { path: 'paragrafo', component: ParagrafoComponent, canActivate: [RequestGuard] },
  { path: 'tbk', component: TbkComponent, canActivate: [RequestGuard] },
  { path: 'document-list', component: DocumentListComponent, canActivate: [RequestGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'view-document', component: ViewDocumentComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'registrado', component: RegistradoComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    StepOneComponent,
    ParagrafoComponent,
    TbkComponent,
    DocumentListComponent,
    LoginComponent,
    ViewDocumentComponent,
    RegistroComponent,
    RegistradoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [
    MatterService,
    ParticipantService,
    IdentityTypeService,
    CountryService,
    UserService,
    DocumentService,
    ParticipantServiceUtil,
    MaritalStatusService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

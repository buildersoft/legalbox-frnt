export class Country {
  id: string;
  name: string;
  demonym: string;
  iso: string;
}

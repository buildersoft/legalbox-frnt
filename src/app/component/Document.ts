import { Paragraph } from './Paragraph';

export class Document {
        id: string;
        identifyParts: string;
        paragraphs: Paragraph[];
        creationAt: string;
        title: string;
        creatorUser: string;
        templateId: string;
        status: string;
}

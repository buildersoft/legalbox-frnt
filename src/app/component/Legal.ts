export class Legal {
  legalName: string;
  fantasyName: string;
  documentIdentityType: string;
  documentIdentityNumber: string;
  address: string;
  legalRepresentativeName: string;
  legalRepresentativeIdentityType: string;
  legalRepresentativeIdentityNumber: string;

};
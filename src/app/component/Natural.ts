export class Natural {
  //  blpid: string;
  names: string;
  lastNames: string;
  address: string;
  documentIdentityType: string;
  documentIdentityNumber: string;
  email: string;
  nationalityId: string;
  nationalityName: string;
  birthday: string;
  maritalStatusId: string;
  maritalStatusName: string;
  ocupation: string;
  phone: string;
};
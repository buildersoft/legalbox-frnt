import { Participant } from './Participant';

export class NewDocument {
  templateId: string;
  userName: string;
  participants: Participant[];
}

export class Paragraph {
    id: string;
    title: string;
    text: string;
    position: number;
    ordinal: string;
    commited: boolean;
}
import { Natural } from './Natural';
import { Legal } from './Legal';


export class Participant {
    blpid: string;
    personType: string;
    natural: Natural = new Natural();
    partType: number;
    partTypeName: string;
    legal: Legal = new Legal();
}

import { Natural } from './Natural';
import { Legal } from './Legal';

export class Person {
  blpid: string;
  personType: string;
  natural: Natural = new Natural();
  legal: Legal = new Legal();

}

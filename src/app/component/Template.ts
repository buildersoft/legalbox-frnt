export class Template {
  id: string;
  name: string;
  legalMatterName: string;
  typePart1: string;
  typePart2: string;
}

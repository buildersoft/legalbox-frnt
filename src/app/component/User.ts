export class User {
  username: string;
  password: string;
  blpid: string;
  names: string;
  lastNames: string;
  address: string;
  documentIdentityType: string;
  documentIdentityNumber: string;
  email: string;
  nationalityId: string;
  nationalityName: string;
  birthday: string;
  roles: string[] = [];
}

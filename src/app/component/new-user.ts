export class NewUser {
    names: string;
    lastNames: string;
    documentIdentityType: string = 'RUT';
    documentIdentityNumber: string;
    birthday: string;
    maritalStatus: string;
    address: string;
    nationalityId: string;
    ocupation: string;
    phone: string;
    mail1: string;
    mail2: string;
    password1: string;
    password2: string;
}

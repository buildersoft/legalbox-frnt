import { Component, OnInit } from '@angular/core';
import { UserService } from '../../api/user.service';
import { Document } from '../../component/Document';
import { DocumentService } from '../../api/document.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css']
})
export class DocumentListComponent implements OnInit {
  documents: Document[] = null;
  documentsFiltred: Document[] = null;

  constructor(private userService: UserService,
    private documentService: DocumentService,
    private router: Router) { }


  ngOnInit(): void {
    let user: string = this.userService.user.username;
    console.log(`User: ${user}`);
    this.documentService
      .listDocuments(user)
      .subscribe(
        response => {
          console.log(response);
          this.documents = response;
          console.log(this.documents);
          
          this.documentsFiltred = this.documents;
        },
        problems => alert(JSON.stringify(problems))

      )

  }

  onClickEdit(id: string): void {
    console.log(id);

  }

  onClickView(id: string): void {
    console.log('view: ' + id);
    sessionStorage.setItem('document', id);
    this.router.navigate(['/view-document']);
  }

  translateStatus(status: string): string {
    switch (status) {
      case 'DRAFT':
        return 'Borrador';
        break;
      case 'SIGNED':
        return 'Firmado';
        break;

      default:
        return '';
        break;
    }
  }


  changeSelectFilter(e: Event) {
    let filterSelect: HTMLSelectElement = e.target as HTMLSelectElement;
    //    alert(filterSelect.value);
    if (filterSelect.value == '') {
      this.documentsFiltred = this.documents;
    } else {
      this.documentsFiltred = this.documents.filter(d => d.status == filterSelect.value);
    }

  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../api/user.service';
import { User } from '../../component/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
  }

  login(): void {
    console.log(this.user);
    if (this.user.username == null || this.user.password == null) {
      alert('Usuario o passsword vacio');
      return;
    }
    this.userService.login(this.user.username, this.user.password).subscribe(
      response => {
        console.log(response);
        this.userService.saveToken(response.access_token);
        this.userService.saveUser(response.access_token);

        this.router.navigate(['/document-list']);
      },
      problems => {
        console.log(problems);
        alert(problems.error.error_description);
      }
    )
  }
}

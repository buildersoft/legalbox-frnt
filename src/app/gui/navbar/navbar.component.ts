import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../api/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  authenticated: boolean;
  username: string;

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    //  this.userService.isAuthenticated
    //this.userService.user.username
    this.authenticated = this.userService.isAuthenticated();
    if (this.authenticated) {
      this.username = this.userService.user.username;
    }
  }

  logout(): void {
    this.userService.logout();
    this.router.navigate(['/']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Document } from '../../component/Document';
import { DocumentService } from '../../api/document.service';
import { Article } from '../../component/Article';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paragrafo',
  templateUrl: './paragrafo.component.html',
  styleUrls: ['./paragrafo.component.css']
})
export class ParagrafoComponent implements OnInit {
  //  documentId: string = null;
  document: Document = new Document();

  constructor(private documentService: DocumentService,
    private router: Router) { }

  ngOnInit(): void {
    this.document.id = sessionStorage.getItem('document');
    // console.log('Document: ' + this.document.id);
    this.documentService
      .readDocument(this.document.id)
      .subscribe(
        response => {
          console.log('Response: ', response);
          this.document = response;
        },
        problems => console.log(JSON.stringify(problems))
      );
  }

  onClickCreateDocument(): void {
    console.log('Actualizando documento');
    this.document.id = this.document.id;

    //    console.log(JSON.stringify(this.document.articles[1]));

    this.documentService.updateDocument(this.document).subscribe(
      response => this.router.navigate(['document-list']),
      problems => console.log(JSON.stringify(problems))
    );
  }

  onChangeArticle(e: any, article: Article): void {
    let newText = e.target.value;
    console.log(JSON.stringify(article) + " -> " + newText);

    this
      .document
      .paragraphs
      .forEach(current => {
        if (current.id === article.id) {
          current.text = newText;
        }

      });

  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/api/user.service';

@Component({
  selector: 'app-registrado',
  templateUrl: './registrado.component.html',
  styleUrls: ['./registrado.component.css']
})
export class RegistradoComponent implements OnInit {
  mailUsuario: string = '';
  nombreUsuario: string = '';
  activeId: string = null;

  constructor(private activateRouted: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit(): void {
    this.nombreUsuario = sessionStorage.getItem('nombreUsuario');
    this.mailUsuario = sessionStorage.getItem('mailUsuario');

    console.log(JSON.stringify(this.activateRouted.queryParams));
    let qParams: any = this.activateRouted.queryParams;
    console.log('x: ' + JSON.stringify(qParams._value.activeId));

    this.activeId = qParams._value.activeId;

    if (this.activeId) {
      this.userService
        .activateUser(this.activeId)
        .subscribe(response => console.log(JSON.stringify(response)),
          problems => console.log(problems)
        );
    }


  }

}

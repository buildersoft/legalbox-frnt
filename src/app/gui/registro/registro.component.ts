import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CountryService } from 'src/app/api/country.service';
import { IdentityTypeService } from 'src/app/api/identityType.service';
import { MaritalStatusService } from 'src/app/api/marital-status.service';
import { UserService } from 'src/app/api/user.service';
import { Country } from './../../component/Country';
import { MaritalStatus } from './../../component/marital-status';
import { NewUser } from './../../component/new-user';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  nationalities: Country[] = [];
  identityTypes: string[] = [];
  maritalStatues: MaritalStatus[] = [];
  newUser: NewUser = new NewUser();
  showSpinner: boolean = false;

  constructor(private countryService: CountryService,
    private identityTypeService: IdentityTypeService,
    private maritalStatusService: MaritalStatusService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this
      .countryService
      .listAll()
      .subscribe(response => {
        this.nationalities = response;
      },
        problems => console.error(problems)
      );

    this.identityTypeService.listAll().subscribe(
      response => this.identityTypes = response,
      problems => console.error(problems)
    );


    this.maritalStatusService.listAll().subscribe(
      response => this.maritalStatues = response,
      problems => console.error(problems)
    );
  }

  saveNewUser(): void {
    this.showSpinner = true;
    this
      .userService
      .createNewUser(this.newUser)
      .subscribe(
        response => {
          console.log('Nuevo Usuario: ' + JSON.stringify(response));
          sessionStorage.setItem('nombreUsuario', response.names);
          sessionStorage.setItem('mailUsuario', response.username);
          this.router.navigate(['registrado']);
        },
        problems => {
          this.showSpinner = false;
          alert(problems.error.message);
          console.log(problems);
        }
      );
  }

}

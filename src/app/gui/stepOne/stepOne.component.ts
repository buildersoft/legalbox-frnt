import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Country } from '../../component/Country';
import { LegalMatter } from '../../component/LegalMatter';
import { Participant } from '../../component/Participant';
import { Person } from '../../component/Person';
import { Template } from '../../component/Template';
import { CountryService } from '../../api/country.service';
import { DocumentService } from '../../api/document.service';
import { IdentityTypeService } from '../../api/identityType.service';
import { MatterService } from '../../api/matter.service';
import { ParticipantService } from '../../api/participant.service';
import { UserService } from '../../api/user.service';
import { ParticipantServiceUtil } from '../../api/participant.util';
import { MaritalStatus } from 'src/app/component/marital-status';
import { MaritalStatusService } from 'src/app/api/marital-status.service';
// import * as moment from 'moment';

@Component({
  selector: 'app-stepOne',
  templateUrl: './stepOne.component.html',
  styleUrls: ['./stepOne.component.css']
})
export class StepOneComponent implements OnInit {
  legalMatters: LegalMatter[];
  templates: Template[];
  template: string = null;
  imPart: string = '';

  blpid: string = ''; // variable temporal
  //  blpid: string = 'CL-B6DD01FB-8007-11EB-9436-0800271E99CA'; // variable temporal

  participants: Participant[] = [];
  participant: Participant = this.participantUtil.createNewParticipant();
  //  participantType: string = null;
  identityTypes: string[];
  countries: Country[];
  maritalStatues: MaritalStatus[];
  partTypes: string[] = null;

  constructor(private matterService: MatterService,
    private participantService: ParticipantService,
    private identityTypeService: IdentityTypeService,
    private countryService: CountryService,
    private userService: UserService,
    private router: Router,
    private documentService: DocumentService,
    private participantUtil: ParticipantServiceUtil,
    private maritalStatusService: MaritalStatusService) {
  }


  ngOnInit(): void {
    /*    console.log(this.userService.user);
        console.log(this.userService.getUserAsPerson());
    */
    this.matterService.listMatter().subscribe(
      response => this.legalMatters = response,
      problems => console.error(problems)
    );
    this.identityTypeService.listAll().subscribe(
      response => this.identityTypes = response,
      problems => console.error(problems)
    );
    this.countryService.listAll().subscribe(
      response => {
        this.countries = response;
        this.participant.natural.nationalityId = this.countries[0].id;
      },
      problems => console.error(JSON.stringify(problems))
    );
    this.maritalStatusService.listAll().subscribe(
      response => this.maritalStatues = response,
      problems => alert(problems.error.error_description)
    );
  }

  onChangeLegalMatter(e: any): void {
    let id: string = e.target.value;
    if (id.length == 0) {
      this.templates = null;
    } else {
      this.matterService//
        .listTemplates(id)//
        .subscribe(
          templates => {
            this.templates = templates;
          }
        );
    }
  }

  onChangeTemplate(ev: any): void {
    let s = ev.target as HTMLSelectElement

    if (s.value.length == 0) {
      this.template = null;
    } else {
      this.template = s.value;

      for (let current of this.templates) {
        if (current.id == this.template) {
          this.partTypes = [current.typePart1, current.typePart2];
        }
      }
      this.participant.partType = 1;
      this.participant.partTypeName = this.partTypes[0];

      this.participantUtil.setPartTypes(this.partTypes);
    }

  }

  onClickFind() {
    this.participantService//
      .find(this.blpid)//
      .subscribe(
        response => {
          console.log('response:' + JSON.stringify(response));
          this.processPersonFind(response);
        },
        error => console.log('error: ' + JSON.stringify(error))
      );
  }

  onChangeIampart(event: Event) {
    let imPartCombo: HTMLSelectElement = event.target as HTMLSelectElement;
    let partType: number = +imPartCombo.value;

    switch (partType) {
      case 0:
        this.participants = this.participantUtil.removeFromParticipants(
          this.participants,
          this.participantUtil.personToParticipant(this.userService.getUserAsPerson())
        );
        break;
      case 1:
      case 2:
        // let participant: Person = this.userService.getUserAsPerson();
        let participant: Participant = this.participantUtil.personToParticipant(this.userService.getUserAsPerson());
        if (this.participantUtil.existsParticipant(this.participants, participant)) {
          this.participants = this.participantUtil.removeFromParticipants(this.participants, participant);
        }
        this.addToParticipants(participant, partType);
        break;
      default:
        break;
    }
  }

  addToParticipants(participant: Participant, partType: number): void {
    //  let participant: Participant = this.participantUtil.personToParticipant(person);
    participant.partType = partType;
    participant.partTypeName = this.partTypes[partType - 1];

    this.participants.push(participant);
  }

  processPersonFind(person: Person): void {
    this.participant = this.participantUtil.personToParticipant(person);

    if (!this.participant) {
      return;
    }


  }

  onClickAddParticipant(): void {
    this.setPartType(this.participant.partType);
    // console.log('onClickAddParticipant -> Participant: ' + JSON.stringify(this.participant));
    console.log('onClickAddParticipant -> Participant: ' + JSON.stringify(this.participant));
    this.participants.push({ ...this.participant });
    this.participant = this.participantUtil.createNewParticipant();
    this.blpid = '';
  }


  onClickSaveAndContinue(): void {
    //let fruits: string[] =
    //    let participants: Participant[] = null;

    console.log('this.participants: ' + this.participants);

    this.documentService.createDocument(this.userService.user.username, this.template, this.participants)
      .subscribe(
        response => {
          console.log('RESPONSE: ' + JSON.stringify(response));
          // console.log('storege:' + Storage);
          sessionStorage.setItem('document', response.id);

          //          console.log(sessionStorage.getItem('document'));

          this.router.navigate(['/tbk']);

        },
        problems => alert('ERROR: ' + JSON.stringify(problems.error.error_description))
      );

    //    this.router.navigate(['/']);
  }

  onChangePartType(e: Event): void {
    let partType = (e.target as HTMLSelectElement).value;
    this.setPartType(+partType);

  }

  setPartType(partType: number): void {
    this.participant.partType = partType;
    this.participant.partTypeName = this.partTypes[(+partType) - 1];

    console.log('setPartType:' + JSON.stringify(this.participant));
  }

  onBlurIdentityNumber() {// JSON.stringify
    //console.log(this.participant.natural.documentIdentityType + ' ' + this.participant.natural.documentIdentityNumber);
    const idType: string = this.participant.natural.documentIdentityType;
    const idNumber: string = this.participant.natural.documentIdentityNumber;

    //    console.log('idNumber!=""' + (idNumber != ''));

    this.setPartType(this.participant.partType);

    if (idNumber != '') {
      this.participantService.findByIdentity(idType, idNumber).subscribe(
        response => {
          if (response != null) {
            this.participant.natural.names = response.natural.names;
            this.participant.natural.lastNames = response.natural.lastNames;
          } else {
            console.log(`Cliente ${idType}:${idNumber} no encontrado.`);
            this.participant = this.participantUtil.createNewParticipant();
            this.participant.natural.documentIdentityType = idType;
            this.participant.natural.documentIdentityNumber = idNumber;
          }
        }
      );
    }
  }

  clickRemoveParticipant(type: string, num: string): void {
    console.log("I:" + type + " " + num);
    let participant: Participant = this.participantUtil.retrieveParticipantByIdentity(this.participants, type, num);

    this.participants = this.participantUtil.removeFromParticipants(this.participants, participant);
    //    this.participantUtil.findParticipant();

  }
}


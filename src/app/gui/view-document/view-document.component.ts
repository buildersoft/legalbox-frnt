import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/api/document.service';
import { Document } from 'src/app/component/Document';

@Component({
  selector: 'app-view-document',
  templateUrl: './view-document.component.html',
  styleUrls: ['./view-document.component.css']
})
export class ViewDocumentComponent implements OnInit {
  document: Document;
  constructor(private documentService: DocumentService) { }

  ngOnInit(): void {
    //    this.document = new Document();
    const id = sessionStorage.getItem('document');

    this.documentService
      .readDocument(id)
      .subscribe(
        response => this.document = response,
        problems => console.log(problems)
      );
  }

}
